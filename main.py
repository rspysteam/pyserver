import logging
import sys

from Server import config
from Server.net import networkHandler
from Server.server import Server

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(name)s: [ %(levelname)s ]: %(message)s')

    socketHandler = networkHandler.SocketHandler
    server = Server()

    address = (config.HOST, config.PORT)

    if len(sys.argv) == 3:
        address = (str(sys.argv[1]), int(sys.argv[2]))

    server.bind_server(address, socketHandler)

    # run server
    server.serve_forever()
