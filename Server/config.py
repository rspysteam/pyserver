# server info
HOST = 'localhost'
PORT = 43594

# Encoder id
ENCODER_HANDSHAKE = 0
ENCODER_LOGIN = 1
ENCODER_GAME = 2

# Decoder id
DECODER_HANDSHAKE = 0
DECODER_LOGIN = 1
DECODER_GAME = 2

STARTING_POSITION = (3093, 3244)

# Player Gender
GENDER_MALE = 1
GENDER_FEMALE = 0
