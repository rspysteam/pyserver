import asyncore
import logging

MAX_BUFFER_SIZE = 4028


class SocketHandler(asyncore.dispatcher):
    def __init__(self, session):
        asyncore.dispatcher.__init__(self, session.get_socket())
        self.logger = logging.getLogger('Client ' + str(session.get_address()))
        self.session = session
        self.buffer = []

    def writable(self):
        return True

    def handle_read(self):
        self.buffer = self.recv(MAX_BUFFER_SIZE)
        self.session.get_decoder().decode(self.buffer)

    def handle_close(self):
        self.logger.info('handle_close()')
        self.session.close()
        self.close()
