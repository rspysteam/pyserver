from Server import config
from Server.game.model.player.playerManager import PlayerManager
from Server.net.codec.game.Game import GameDecoder
from Server.net.codec.game.Game import GameEncoder
from Server.net.codec.handshake.Handshake import HandshakeDecoder
from Server.net.codec.handshake.Handshake import HandshakeEncoder
from Server.net.codec.login.Login import LoginDecoder
from Server.net.codec.login.Login import LoginEncoder


class Session:
    def __init__(self, channel_info, player=None):
        self.socket = channel_info[0]
        self.address = channel_info[1]
        self.uid = None
        self.encoder = None
        self.decoder = None
        self.encoder_encryption = None
        self.decoder_encryption = None
        self.set_decoder(config.DECODER_HANDSHAKE)
        self.last_recv_packet = []
        self.last_send_packet = []

    def set_encryptionDecoder(self, encryption):
        self.decoder_encryption = encryption

    def get_encryptionDecoder(self):
        return self.decoder_encryption

    def set_encryptionEncoder(self, encryption):
        self.encoder_encryption = encryption

    def get_encryptionEncoder(self):
        return self.encoder_encryption

    def get_uid(self):
        return self.uid

    def set_uid(self, uid):
        self.uid = uid

    def get_socket(self):
        return self.socket

    def get_address(self):
        return self.address

    def get_encoder(self):
        return self.encoder

    def get_decoder(self):
        return self.decoder

    def set_encoder(self, encoder):
        if encoder == config.ENCODER_HANDSHAKE:
            self.encoder = HandshakeEncoder(self)
        elif encoder == config.ENCODER_LOGIN:
            self.encoder = LoginEncoder(self)
        elif encoder == config.ENCODER_GAME:
            self.encoder = GameEncoder(self)

    def set_decoder(self, decoder):
        if decoder == config.DECODER_HANDSHAKE:
            self.decoder = HandshakeDecoder(self)
        elif decoder == config.DECODER_LOGIN:
            self.decoder = LoginDecoder(self)
        elif decoder == config.DECODER_GAME:
            self.decoder = GameDecoder(self)

    def write(self, data):
        self.socket.send(data)

    def write_e(self, data):
        self.encoder.write(data)

    # TODO save player and close the connection save
    def close(self):
        for player in PlayerManager.get_players():
            if player.get_uid() == self.uid:
                player.save_logout()
                self.socket.close()
                return