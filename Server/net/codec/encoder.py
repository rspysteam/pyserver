import logging


class Encoder:
    encryption = None

    def __init__(self, session):
        self.logger = logging.getLogger('Encoder')
        self.socket = session.get_socket()
        self.session = session
        self.encryption = session.get_encryptionEncoder()

    def write(self, packet):
        if packet.is_encrypted():
            encryption_val = self.encryption.next_int()
            opcode = packet.get_data()[0]
            self.logger.debug("Encryption val [{}] masked [{}] ".format(encryption_val, (encryption_val & 0xff)))
            packet.set_byte(0, bytes([(opcode + encryption_val) & 0xff]))

        data = packet.get_data()
        self.logger.debug('sending packet ->  size {%s} ', packet.length())
        self.socket.send(data)

    def update(self, encoder, decoder):
        self.session.set_encoder(encoder)
        self.session.set_decoder(decoder)
