from Server.net.codec.decoder import Decoder
from Server.net.codec.encoder import Encoder
from Server.util.BufferUtils import ByteBuffer

class GameDecoder(Decoder):
    def __init__(self, session):
        self.session = session
        Decoder.__init__(self, session)

    def attach_player(self, player):
        self.player = player

    def decode(self, byte_buffer):
        encryption_val = self.encryption.next_int()
        packet = self.construct_packet(byte_buffer)
        opcode = (packet.read_byte() - encryption_val) & 0xff
        size = packet.length()
        if opcode == 164:
            buffer = packet.get_buffer()
            size = packet.length()
            path_size = int((packet.read_byte() - 3) / 2)
            x_base = packet.read_byte() - 128 | (packet.read_byte() << 8)  # rs special datatype

            steps_x = [steps for steps in range(path_size)]
            steps_y = [steps for steps in range(path_size)]

            for steps in range(1, path_size):
                steps_x[steps] = packet.read_byte()
                steps_y[steps] = packet.read_byte()
            y_base = packet.read_byte() | (packet.read_byte() << 8)  # rs special datatype

            _steps = [steps for steps in range(path_size)]
            for steps in range(1, path_size):
                _steps[steps] = [steps_x[steps] + x_base, steps_y[steps] + y_base]

            key = packet.read_byte()

            self.logger.info(
                "path_size {}, x_base {}, y_base {}, key {}, steps {}".format(path_size, x_base, y_base,
                                                                              key, _steps))

        if opcode == 0 and size > 0:
            for i in range(4):
                encryption_val = self.encryption.next_int()
                self.logger.debug("opcode : {}, byte : {}".format(opcode, (packet.read_byte() - encryption_val) & 0xff))
            encryption_val = self.encryption.next_int()
            intI = packet.read_int()
            self.logger.debug("opcode : {}, int? : {}, buffer : {}".format(opcode, intI, (
                packet.read_byte() - encryption_val) & 0xff))
        elif opcode == 0:
            pass
        elif opcode == 241 and size > 20:
            for _ in range(4):
                self.logger.debug("opcode : {}, byte : {}".format(opcode, packet.read_byte()))
            encryption_val = self.encryption.next_int()
            self.logger.debug("opcode : {}, byte : {}".format(opcode, (packet.read_byte() - encryption_val) & 0xff))
        else:
            self.logger.debug("opcode : {}, size : {}".format(opcode, packet.length()))


class GameEncoder(Encoder):
    def __init__(self, session):
        self.session = session
        Encoder.__init__(self, session)

    def send_region(self, player):
        packet = ByteBuffer(opcode=73, encrypt=True)

        packet.write_byte((player.get_position().get_region_x() + 6) >> 8)
        packet.write_byte((player.get_position().get_region_x() + 6) + 128 >> 8)
        packet.write_short(player.get_position().get_region_y() + 6)

        self.write(packet)

    def send_all_side_bars(self):
        side_bars = (
            (1, 3917), (2, 638), (3, 3213), (4, 1644), (5, 5608), (6, 1151),
            # 6 is spell book which can be changed for normal to ancient, 0/1
            (8, 5065), (9, 5715), (10, 2449), (11, 904), (12, 147), (13, 962),
            (0, 2423)
        )
        for side_bar in side_bars:
            self.send_side_bar(side_bar)

    def send_side_bar(self, side_bar):
        packet = ByteBuffer(opcode=71, encrypt=True)
        packet.write_short(side_bar[1])
        packet.write_byte(side_bar[0] + 128)
        self.write(packet)

    def encode(self):
        pass
