import random

from Server import config
from Server.net.codec.decoder import Decoder
from Server.net.codec.encoder import Encoder
from Server.util import misc
from Server.util.BufferUtils import ByteBuffer


class HandshakeDecoder(Decoder):
    def __init__(self, session):
        self.session = session
        Decoder.__init__(self, session)

    def decode(self, byte_buffer):
        packet = self.construct_packet(byte_buffer)

        id = packet.read_byte()  # 14
        nameHash = packet.read_byte()  # 15 the namehash???

        encoder = HandshakeEncoder(self.session)
        encoder.encode()


class HandshakeEncoder(Encoder):
    def __init__(self, session):
        self.session = session
        Encoder.__init__(self, session)

    def encode(self):
        packet = ByteBuffer()

        seed1 = misc.convert_byte_format(random.random() * 99999999, '>f', '>i')[0]
        seed2 = misc.convert_byte_format(random.random() * 99999999, '>f', '>i')[0]
        session_key = (seed1 << 32) + seed2

        for _ in range(9):
            packet.write_byte(0)

        packet.write_long(session_key, byte_order='big', signed=False)
        self.write(packet)

        ''' change encoder & decoder '''
        self.logger.debug('changing form decoder & encoder ')
        self.update(config.ENCODER_LOGIN, config.DECODER_LOGIN)
