import logging

from Server.util import BufferUtils


class Decoder:
    def __init__(self, session):
        self.logger = logging.getLogger("Decoder")
        self.encryption = session.get_encryptionDecoder()

    def construct_packet(self, byte_buffer):
        packet = BufferUtils.ByteBuffer(byte_buffer)
        return packet
