from Server import config
from Server.game.model.player.player import Player
from Server.game.model.player.playerManager import PlayerManager
from Server.game.model.world.world import World
from Server.net.PySAAC import IsaacRandom
from Server.net.codec.decoder import Decoder
from Server.net.codec.encoder import Encoder
from Server.net.codec.login import OPCODE
from Server.util import misc
from Server.util.BufferUtils import ByteBuffer


class LoginDecoder(Decoder):
    def __init__(self, session):
        self.session = session
        self.encoder = LoginEncoder(self.session)
        Decoder.__init__(self, session)

    def decode(self, byte_buffer):
        packet = self.construct_packet(byte_buffer)

        connecting_status = packet.read_byte()
        packet_size = packet.read_byte()
        buffer_size = packet.read_byte()
        int255 = packet.read_byte()
        revision = packet.read_short(byteorder='big')
        client_version = packet.read_byte()
        crc_val = []

        for _ in range(9):
            val = packet.read_int()
            crc_val.append(val)

        b10 = packet.read_byte()

        isaac_seed = [_ for _ in range(4)]
        isaac_seed[0] = packet.read_int()
        isaac_seed[1] = packet.read_int()
        isaac_seed[2] = packet.read_int()
        isaac_seed[3] = packet.read_int()

        encryption = IsaacRandom()
        encryption.seed(isaac_seed)

        self.session.set_encryptionDecoder(encryption)

        for index in range(len(isaac_seed)):
            isaac_seed[index] += 50

        encryption = IsaacRandom()
        encryption.seed(isaac_seed)

        self.session.set_encryptionEncoder(encryption)
        uid = packet.read_int()

        username = packet.read_string()
        password = packet.read_string()

        self.logger.debug('connecting_status %s', connecting_status)
        self.logger.debug('packet_size %s', packet_size)
        self.logger.debug('buffer_size %s', buffer_size)
        self.logger.debug('int255 %s', int255)
        self.logger.debug('revision %s', revision)
        self.logger.debug('client_version %s', client_version)
        self.logger.debug('encryption seed %s', isaac_seed)
        self.logger.debug('crc_val %s', crc_val)
        self.logger.debug('b10 %s', b10)
        self.logger.debug('uid %s', uid)
        self.logger.debug('username %s', username)
        self.logger.debug('password %s', password)

        ''' initialize player and add it to the game'''
        player_credentials = (username, misc.string_to_long(username), password)
        self.logger.debug('username_hash -> {}'.format(misc.string_to_long(username)))
        #  we use the uid to find the player when the session is closed()
        #  see -> session.close()
        #  if there is a better way I would like to know.
        self.session.set_uid(uid)

        self.validate_player(player_credentials)

    def validate_player(self, player_credentials):
        username, username_hash, password = player_credentials
        self.logger.debug('{} attempting to login with username "{}" '.format(self.session.get_address()[0], username))

        if not misc.validate_string_char(username):
            self.encoder.encode(opcode=OPCODE.INVALID_CREDENTIALS)
            return self.logger.debug(
                '{} failed to login with username "{}" '.format(self.session.get_address()[0], username))

        if PlayerManager.is_banned(username) or PlayerManager.ip_is_banned(self.session.get_address()[0]):
            self.encoder.encode(opcode=OPCODE.ACC_DISABLED)
            return self.logger.debug(
                '{} failed to login with banned account "{}" '.format(self.session.get_address()[0], username))

        if PlayerManager.player_exist(username):
            player = PlayerManager.load_player(username)
            uid = self.session.get_uid()
            player.set_session(self.session, uid)
        else:
            player = Player(player_credentials, self.session)

        if player.get_password() != password:
            self.encoder.encode(opcode=OPCODE.INVALID_CREDENTIALS)
            return self.logger.debug(
                '{} failed to login with an invalid password "{}" '.format(self.session.get_address()[0], password))

        self.encoder.encode(opcode=OPCODE.LOGIN_SUCCESS)
        World.register(player)


class LoginEncoder(Encoder):
    def __init__(self, session):
        self.session = session
        Encoder.__init__(self, session)

    def encode(self, opcode):
        packet = ByteBuffer()

        '''At the beginning and end of the login procedure,
         we send different values to the client to allow or deny a login.
         The various values show different messages on the login box on the client or do something internally.
         
         link for more info : http://rsps.wikia.com/wiki/317_Protocol#Response_Codes
         '''

        packet.write_byte(opcode)  # response code
        ''' player status
            0 : Signifies that this player is a normal player. 
            1 : Signifies that this player is a player moderator. 
            2 : ignifies that this player is an administrator.
        '''
        packet.write_byte(2)  # player status

        ''' If set to 1, information about mouse movements etc.
        are sent to the server. Suspected bot accounts are flagged. '''
        packet.write_byte(0)

        ''' send byte_buffer to the client '''
        self.write(packet)

        ''' change encoder & decoder '''
        if opcode == OPCODE.LOGIN_SUCCESS:
            self.update(config.ENCODER_GAME, config.DECODER_GAME)
