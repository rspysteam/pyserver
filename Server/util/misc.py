import random
import re
import struct


def random_string(minlen, maxlen, crypto=False, alphanum=True):
    stringchars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890'
    if not alphanum:
        stringchars += '!@#$%^&*()-_=+'
    random_function = random.SystemRandom().randint if crypto else random.randint
    string_length = random_function(minlen, maxlen)
    rstring = ''
    for _ in range(string_length):
        idx = random_function(0, len(stringchars) - 1)
        rstring += stringchars[idx]
    return rstring


def string_to_long(string):
    l = i = 0
    while i < len(string) and i < 12:
        l *= 37
        char = string[i]
        if 'A' <= char <= 'Z':
            l += (1 + ord(char)) - 65
        elif 'a' <= char <= 'z':
            l += (1 + ord(char)) - 97
        elif '0' <= char <= '9':
            l += (27 + ord(char)) - 48
        i += 1
    while l % 37 is 0 and l is not 0:
        l /= 37
    return l


def validate_string_char(string):
    if len(string) > 0:
        valid_char = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890'
        return False if len([char for char in string if char not in valid_char]) > 0 else True
    return False


def replace_str_char(string, idx, replacement):
    if type(string) is str:
        replaced = '{}{}{}'.format(string[:idx], replacement, string[idx + 1:])
    elif type(string) is bytes:
        replaced = b''.join([string[:idx], replacement, string[idx + 1:]])
    else:
        raise TypeError("Cannot replace a character in type {}".format(type(string)))
    return replaced


def chunk_string(string, chunksize):
    search = '.{1,' + str(chunksize) + '}'
    if type(string) is bytes:
        search = search.encode()
    return re.findall(search, string)


def extract_fields_from_dict(extraction_dict, extraction_fields):
    extracted = []
    for field in extraction_fields:
        extracted.append(extraction_dict.pop(field))
    return tuple(extracted)


def length(data):
    if type(data) is int:
        return int(data.bit_length() + 7) // 8
    else:
        return len(data)


def delete_fields_from_dict(deletion_dict, deletion_fields):
    for field in deletion_fields:
        deletion_dict.pop(field)


def bytes_to_str(byte_string):
    reg_string = ''
    for byte in byte_string:
        reg_string += chr(ord(byte))
    return repr(reg_string)


def pack_bytes_to_format(byte_data, pack_format, trailing_data=False):
    pack_size = struct.calcsize(pack_format)
    packed_bytes = struct.pack(pack_format, byte_data[:pack_size] if type(byte_data) == bytes else byte_data)
    return packed_bytes


def unpack_bytes_to_format(byte_data, unpack_format, trailing_data=False):
    unpack_size = struct.calcsize(unpack_format)
    unpacked_bytes = struct.unpack(unpack_format, byte_data[:unpack_size] if type(byte_data) == bytes else byte_data)
    return (unpacked_bytes, byte_data[unpack_size:]) if trailing_data else unpacked_bytes


def convert_byte_format(byte_data, from_format, to_format, trailing_data=False):
    packed_data = pack_bytes_to_format(byte_data, from_format, trailing_data)
    converted_bytes = unpack_bytes_to_format(packed_data, to_format, trailing_data)
    return converted_bytes