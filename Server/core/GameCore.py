import logging
from threading import Thread
from time import sleep

from Server.core import manager
from Server.game.model.player.playerUpdater import PlayerUpdater
from Server.game.model.world.world import World


class GameEngine(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.logger = logging.getLogger('Game Engine')

    def run(self):
        while manager.isRunning:
            for player in World.get_player_manager().get_players():
                PlayerUpdater.update_player(player)
            sleep(0.6)
