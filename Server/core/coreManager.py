from Server.core import manager
from Server.core.GameCore import GameEngine


class CoreManager:
    @staticmethod
    def init():
        manager.isRunning = True
        gt = GameEngine()
        gt.start()

    @staticmethod
    def shutdown():
        manager.isRunning = False
