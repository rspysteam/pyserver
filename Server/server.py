import asyncore
import logging
import socket
import sys

from Server.core.coreManager import CoreManager
from Server.net.session import Session


class Server(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.logger = logging.getLogger('Server')
        self.logger.info("Starting Server")

    def bind_server(self, addr, handler):
        try:
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.bind(addr)
            self.address = self.socket.getsockname()
            self.handler = handler
            self.logger.info('binding to %s', self.address)
            self.listen(5)
        except socket.error as e:
            self.logger.error(str(e.args[0]) + ", " + e.args[1])
            self.logger.error('Closing Socket: ' + str(addr))
            sys.exit()

    @classmethod
    def serve_forever(cls):
        CoreManager.init()
        asyncore.loop()

    def handle_accept(self):
        channel_info = self.accept()  # (sock, addr)
        session = Session(channel_info)

        if channel_info is not None:
            self.logger.info('handle_accept() -> %s', channel_info[1])
            self.handler(session)
