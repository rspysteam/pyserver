import json

from Server.util import BufferUtils



"""
converting from
    Item ID - unsigned short
    Item Name - RSString
    Item Description - RSString
    Item Bonuses - Signed Short per bonus if item has any non-0 bonuses, else 4e4f

to
    Item ID - 15 bits
    Item Name - String delimited with bits 1010, saves 4 bits per delimit
    Item Description - Same as Item Name
    Item bonuses - 13 bits (first 4 for bonus id 0-11, last 9 for value -255 to 255). If bonus id is 12 read next item

"""


def pack_itemdef(item_buffer, itemdef_object):
    """
    Format: Item ID (unsigned short), Item Name (RSString), Item Description (RSString), Item Price (Signed Int), Bonuses (12-len Unsigned short array) or NO if bonus-less
    """
    item_buffer.write_short(itemdef_object.item_id, byte_order='big', signed=False)
    item_buffer.write_string(itemdef_object.item_name)
    item_buffer.write_string(itemdef_object.item_description)
    item_buffer.write_int(itemdef_object.item_price, byte_order='big', signed=True)
    if itemdef_object.item_bonuses:
        for bonus in itemdef_object.item_bonuses:
            item_buffer.write_short(bonus, byte_order='big', signed=True)
    else:
        item_buffer.write_bytes(b'\x4e\x4f')
    return item_buffer.take_data()


def pack_db(item_db, filepath, filename):
    with open(filepath+filename, 'wb') as item_bin:
        for item_id in item_db:
            item = item_db[item_id]
            bytestring = b''
            bytestring += item_id.to_bytes(2, byteorder='big', signed=False)
            bytestring += item["Name"].encode()+b'\n'
            bytestring += item["Description"].encode()+b'\n'
            bytestring += item["Price"].to_bytes(4, byteorder='big', signed=True)
            bytestring += b''.join([bonus.to_bytes(2, byteorder='big', signed=True) for bonus in item["Bonuses"]]) if "Bonuses" in item else b'\x4e\x4f'
            item_bin.write(bytestring)


def pack_itemdef_db(itemdef_db, filepath, filename):
    itemdef_buffer = BufferUtils.ByteBuffer()
    with open(filepath+filename, 'wb') as idef_bin:
        for item_id in itemdef_db:
            itemdef = itemdef_db[item_id]
            idef_bin.write(pack_itemdef(itemdef_buffer, itemdef))


def unpack_itemdef(item_buffer):
    item_id = item_buffer.read_short(signed=False)
    item_name = item_buffer.read_string()
    item_description = item_buffer.read_string()
    item_price = item_buffer.read_int(signed=True)
    check = item_buffer.read_short(signed=True)
    if check == 20047:
        item_bonuses = None
    else:
        item_bonuses = [check]
        for _ in range(11):
            item_bonuses.append(item_buffer.read_short(signed=True))
    return ItemDef(item_id, item_name, item_description, item_price, item_bonuses)


def load_items_from_json(filepath, filename, itemdefs=True):
    with open(filepath+filename, 'r') as item_file:
        item_db = json.loads(item_file.read())
    if itemdefs:
        return {
            int(item_id): ItemDef(
                item_id=int(item_id),
                item_name=item_db[item_id]["Name"],
                item_description=item_db[item_id]["Description"],
                item_price=int(item_db[item_id]["Price"]),
                item_bonuses=[int(x) for x in item_db[item_id]["Bonuses"]] if "Bonuses" in item_db[item_id] else None
            ) for item_id in item_db
        }
    else:
        return {int(item_id): item_db[item_id] for item_id in item_db}


def load_items_from_bin(filepath, filename):
    with open(filepath+filename, 'rb') as item_bin:
        data = item_bin.read()
    item_db_buffer = BufferUtils.ByteBuffer(data)
    item_db = {}
    while item_db_buffer:
        item_def = unpack_itemdef(item_db_buffer)
        item_db[item_def.item_id] = item_def
    return item_db


class ItemDef:
    def __init__(self, item_id, item_name, item_description,  item_price, item_bonuses
                 ):
        self.item_id = item_id
        self.item_name = item_name
        self.item_description = item_description
        self.item_price = item_price
        if item_bonuses is None:
            # TODO: Decide if it should be None or a 0 filled 12-tuple
            # fill a 12-tuple with 0 for the sake of readability.
            self.item_bonuses = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        elif type(item_bonuses) in (list, tuple):
            if len(item_bonuses) == 12:
                self.item_bonuses = tuple(item_bonuses)
            else:
                raise ValueError("Item bonus length must be 12, not {}".format(len(item_bonuses)))
        else:
            raise TypeError("Invalid item_bonuses type for item ({} {}). Must be list or tuple, not {}".format(self.item_name, self.item_id, type(item_bonuses)))


class FullItemDef(ItemDef):
    # TODO: Decide if other params that can reduce code size need to be added such as wield_anim, stand_anim, etc.
    def __init__(self, item_id, item_name, item_description, item_price, item_bonuses, *,
                 tradeable, stackable, noted, opponoted_id, wieldable, requirements, slot_id
                 ):
        ItemDef.__init__(self, item_id, item_name, item_description, item_price, item_bonuses)
        self.tradeable = bool(tradeable)
        if noted:
            self.noted = True
            self.stackable = True
        else:
            self.noted = False
            self.stackable = stackable
        self.opponoted_id = opponoted_id
        self.wieldable = bool(wieldable)
        if type(requirements) is None:
            # TODO: Decide if it should stay None or a 0 filled 24-tuple
            self.requirements = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        elif type(requirements) in (list, tuple):
            if len(requirements) == 24:
                self.requirements = tuple(requirements)
            else:
                raise ValueError("Item bonus length must be 24, not {}".format(len(item_bonuses)))
        else:
            raise TypeError("Invalid requirements type for item ({} {}). Must be list or tuple, not {}".format(self.item_name, self.item_id, type(requirements)))
        self.slot_id = slot_id
