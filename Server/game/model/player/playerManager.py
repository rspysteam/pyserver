import logging
import os

import dill as pickle

from Server.game.model.world.constants import *


class PlayerManager(object):
    logger = logging.getLogger('PlayerManager')

    with open('data/banned_users.txt', 'r+') as banned_user_file:
        banned_users = [username.lower().strip() for username in banned_user_file.readlines()]

    with open('data/banned_ips.txt', 'r+') as banned_ip_file:
        banned_ips = [ip.strip() for ip in banned_ip_file.readlines()]

    @classmethod
    def get_player(cls, username):
        """
        an class_method that returns the player obj

        :param username:
            username (str): the player username.

        :return
            object (Player): the Player Object [ Server.game.player.player > class Player ]

        """
        if hasattr(online_player_dictionary, username):
            return online_player_dictionary[username]

    @classmethod
    def get_players(cls):
        """
        an class_method that that returns the list ( online_players )

        :return
            online_players (list): the list that contains online player

        """
        return online_players

    @classmethod
    def register(cls, player):
        """
        an class_method that adds the player to list ( online_players ) and dictionary ( online_player_dictionary ).
        :param player:
            player (obj): The player Object
        :return
            bool: true when done executing the code.

        """
        username = player.get_username()
        if not cls.get_player(username):
            online_players.append(player)
            online_player_dictionary[username] = player
            cls.logger.info('Done registering Player : ' + username)
        else:
            cls.logger.info('Player "{}" is already registered'.format(username))
        return True

    @classmethod
    def remove_player(cls, player):
        """
        remove_player
            Function that removes the player from the dictionary ( online_player_dictionary ) and the list ( online_players )

        :param player:
            Player whose removed from the dictionary ( online_player_dictionary ) and the list ( online_players )
        """
        username = player.get_username()
        if hasattr(online_player_dictionary, username):
            del online_player_dictionary[username]

        online_players.remove(player)


    @classmethod
    def is_online_player(cls, username):
        """
        an class_method that check if the player is online.

         :param username:
            username (str): the player username.

         :return
            bool: True if player exist in dictionary ( online_player_dictionary ).
            bool: False username doesn't exist in the dictionary ( online_player_dictionary ).

        """
        if username in online_player_dictionary:
            return True
        else:
            return False

    @classmethod
    def is_banned(cls, username):
        """
        is_banned
            Function that returns True or False based on whether the player's username is in data/banned_users.txt

        :param username:
            Player whose username we are checking if it is in data/banned_users.txt

        :return bool:
            True if player's username is in data/banned_users.txt
            False otherwise
        """

        return username.lower() in cls.banned_users

    @classmethod
    def ip_is_banned(cls, ip_address):
        """
        is_banned
            Function that returns True or False based on whether the ip address is in data/banned_ips.txt

        :param ip_address:
            Ip address we are checking if it is in data/banned_ips.txt

        :return bool:
            True if the ip is in data/banned_ips.txt
            False otherwise
        """
        return ip_address in cls.banned_ips

    @classmethod
    def player_exist(cls, username):
        return username.lower() in [entry.name.split('.')[0].lower() for entry in os.scandir('data/players')]

    @classmethod
    def save_player(cls, player):
        username = player.get_username()

        # this will fix the socket error
        player.set_session(None, None)

        pickle.dump(player, open('data/players/' + username + '.ply', 'bw'))
        cls.logger.debug('Done saving Player : ' + username)

    @classmethod
    def load_player(cls, username):
        player = pickle.load(open('data/players/' + username + '.ply', 'rb'))
        return player
