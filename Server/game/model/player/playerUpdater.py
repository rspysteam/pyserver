import logging
from Server.util.BufferUtils import BitBuffer
from Server.util.BufferUtils import ByteBuffer


class PlayerUpdater(object):

    logger = logging.getLogger("PlayerUpdater")

    @staticmethod
    def updateApperance(player, bytebuffer):
        appearance = player.appearance
        apperance_block = ByteBuffer()

        apperance_block.write_byte(appearance.gender)  # Gender
        apperance_block.write_byte(0)  # Head Icon

        apperance_block.write_short(0x100 + appearance.hair_style)  # Head slot
        apperance_block.write_byte(0)  # Back slot
        apperance_block.write_byte(0)  # Neck slot
        apperance_block.write_short(0x200 + 4151)  # Right-hand slot
        apperance_block.write_short(0x100 + appearance.torso_style)  # Torso slot
        apperance_block.write_byte(0)  # Left-hand slot
        apperance_block.write_short(0x100 + appearance.arms_style)  # Arms? slot
        apperance_block.write_short(0x100 + appearance.legs_style)  # Legs slot
        apperance_block.write_short(0x100 + appearance.hair_style)  # Hair slot
        apperance_block.write_short(0x100 + appearance.hands_style)  # Hand/Wrist slot
        apperance_block.write_short(0x100 + appearance.feet_style)  # Boots slot
        apperance_block.write_byte(0)  # Facial hair slot
        PlayerUpdater.logger.debug("Appended equipment to {}'s appearance block".format(player.username))

        # colors
        apperance_block.write_byte(appearance.hair_color)
        apperance_block.write_byte(appearance.torso_color)
        apperance_block.write_byte(appearance.leg_color)
        apperance_block.write_byte(appearance.feet_color)
        apperance_block.write_byte(appearance.skin_color)
        PlayerUpdater.logger.debug("Appended colors to {}'s appearance block".format(player.username))

        # animation
        apperance_block.write_short(0x332)  # Stand animation
        apperance_block.write_short(0x337)  # Standing turn animation
        apperance_block.write_short(0x333)  # Walking animation
        apperance_block.write_short(0x334)  # Turn 180 animation
        apperance_block.write_short(0x335)  # Turn right animation
        apperance_block.write_short(0x336)  # Turn left animation
        apperance_block.write_short(0x338)  # Run animation
        PlayerUpdater.logger.debug("Appended animations to {}'s appearance block".format(player.username))

        apperance_block.write_long(player.username_hash, byte_order='big')
        apperance_block.write_byte(69)  # combat level
        apperance_block.write_short(65535)  # skill level
        PlayerUpdater.logger.debug("Appended Userhash and levels to {}'s appearance block".format(player.username))

        bytebuffer.write_byte(-len(apperance_block.get_buffer()), signed=True)
        bytebuffer.append_bytes(apperance_block.get_buffer())
        PlayerUpdater.logger.debug("{}'s appearance block (len {}): {}".format(player.username, len(bytebuffer), bytebuffer.buffer))

    @staticmethod
    def update_player(player):
        PlayerUpdater.logger.debug("Updating {}".format(player.username))
        bytebuf = ByteBuffer(opcode=81, encrypt=True)
        PlayerUpdater.logger.debug("len of bytebuf: {}".format(len(bytebuf)))
        bytebuf.write_short(0)  # c
        bit_buffer = BitBuffer(b'', auto_update=False, return_separate_bytes=False, auto_pad=False, return_type=bytes)

        # send the local player movement

        # our player needs an movement update
        bit_buffer.write_bit(True)  # currentlyUpdating

        bit_buffer.write_bit(3, amount=2)  # the type of update
        bit_buffer.write_bit(0, amount=2)  # height of the plane
        bit_buffer.write_bit(True)  # discardMovementQueue
        bit_buffer.write_bit(True)  # update required
        bit_buffer.write_bit(player.get_position().get_local_y(), amount=7)
        bit_buffer.write_bit(player.get_position().get_local_x(), amount=7)

        bit_buffer.write_bit(0, amount=8)

        bit_buffer.write_bit(2047, amount=11)
        bytebuf.append_bytes(bit_buffer.get_bytes())
        bytebuf.write_byte(16)
        PlayerUpdater.updateApperance(player, bytebuf)
        bytebuf[1], bytebuf[2] = (len(bytebuf)-3).to_bytes(2, 'big')
        player.session.encoder.write(bytebuf)
