SLOTS = {
    "HEAD": 0,
    "BACK": 1,
    "NECK": 2,
    "RIGHTHAND": 3,
    "TORSO": 4,
    "LEFTHAND": 5,
    "ARMS": 6,
    "LEGS": 7,
    "HAIR": 8,
    "HANDS": 9,
    "FEET": 10,
    "FACIAL_HAIR": 11,
    "RING": 12,
    "ARROWS": 13
}
class Equipment:
    def __init__(self):
        pass