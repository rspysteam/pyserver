import logging

from Server import config
from Server.game.model.inv.inventoryManager import Inventory
from Server.game.model.player.appearance import Appearance
from Server.game.model.player.playerManager import PlayerManager
from Server.game.model.world.position import Position


class Player(object):
    logger = logging.getLogger('Server')

    def __init__(self, login_credential, session=None):
        self.username, self.username_hash, self.password = login_credential
        self.session, self.uid = (session, session.get_uid())
        self.position = Position(config.STARTING_POSITION[0], config.STARTING_POSITION[1])
        self.inventory_manager = Inventory()
        self.appearance = Appearance()
        self.head_icon = -1
        self.run_energy = 100
        self.gender = config.GENDER_MALE
        self.appearance_update_flag = False

    def init(self):
        self.get_session().get_encoder().send_region(self)
        self.get_session().get_encoder().send_all_side_bars()

    def send_region(self, region=None):
        pass

    def set_position(self, position):
        self.position = position

    def get_position(self):
        return self.position

    def get_region(self):
        return self.position.get_region_x(), self.position.get_region_y()

    def get_session(self):
        return self.session

    def set_session(self, session, uid):
        self.session = session
        self.uid = uid

    def get_uid(self):
        return self.uid

    def set_uid(self):
        return self.uid

    def get_inventory_manager(self):
        return self.inventory_manager

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_gender(self):
        return self.get_gender

    def set_gender(self, gender):
        self.gender = gender

    # remove player from player updating/region updating.
    # TODO close trade and end all player interaction
    def save_logout(self):
        PlayerManager.remove_player(self)
        PlayerManager.save_player(self)

    def __str__(self):
        return str(self.username) + " : " + str(self.password)
