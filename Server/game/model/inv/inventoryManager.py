from Server.game.model.inv.inventoryConstants import *


# TODO add more functions to the inventory like ( swamp, update, refresh )
class Inventory(object):
    #11682

    def __init__(self):
        self.capacity = MAX_INVENTORY_CAPACITY
        self.used_slots = 0
        self.items = {slot: {0: 0} for slot in range(self.capacity)}  # {slot0: {item_id: item_amount}, ..., slot27:...}

    def add_item(self, item, amount=1, slot=None):
        if 27 < slot < 0:
            raise IndexError("slot must be in inclusive range: 0-27")
        if False:
            pass


    def remove_item(self, item): pass

    def clear_inventory(self):
        self.used_slots = 0
        self.items = [item for item in range(self.capacity)]

    def contains(self, item_id):
        for slot in self.items:
            if item_id in slot:
                return True

    def get_free_slots(self):
        return self.capacity - self.used_slots

    def __len__(self):
        return len(self.items)
