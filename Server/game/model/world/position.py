import math


class Position(object):
    # should [y] be [z], look at this -> https://www.rune-server.ee/runescape-development/rs2-server/informative-threads/502839-coordinate-misunderstanding.html
    # he is right though
    def __init__(self, x, y, height=0):
        self.x = x
        self.y = y
        self.height = height

    def __str__(self):
        return "x: {}, y: {}, height: {}, local_x: {}, local_y: {}, region_x: {}, region_y: {}".format(
            self.x, self.y, self.height, self.get_local_x(), self.get_local_y(),
            self.get_region_x(), self.get_region_y()
        )

    def get_region_y(self):
        return (self.y >> 3) - 6

    def get_region_x(self):
        return (self.x >> 3) - 6

    def get_region(self):
        return self.x >> 3 - 6, self.y >> 3 - 6

    def get_local_y(self, position=None):
        if type(position) == object:
            return position.get_y() - 8 * position.get_region_y()

        return self.y - 8 * self.get_region_y()

    def get_local_x(self, position=None):
        if type(position) == object:
            return position.get_x() - 8 * position.get_region_x()

        return self.x - 8 * self.get_region_x()

    def get_y(self):
        return self.y

    def get_x(self):
        return self.x

    def get_height(self):
        return self.height

    def isWithinDistance(self, position, distance):
        delta_x = math.fabs(self.x - position.get_x())
        delta_y = math.fabs(self.y - position.get_y())

        return delta_x <= distance and delta_y <= distance


if __name__ == '__main__':
    pos = Position(3211, 3424)
    print(pos)
