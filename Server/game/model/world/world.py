from Server.game.model.player.playerManager import PlayerManager


class World(object):
    player_manager = PlayerManager

    @classmethod
    def get_player_manager(cls):
        return cls.player_manager

    @classmethod
    def register(cls, player):
        player.init()
        cls.get_player_manager().register(player)
