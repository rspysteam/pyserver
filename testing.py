from Server.game.model.item import ItemDefinition as ItemDef
from Server.util import stopwatch


def pack_unpack_test(passes):
    timer = stopwatch.Timer()

    times = {
        "itemdef": {
            "pack": []
        },
        "items": {
            "pack": []
        }
    }

    for _ in range(passes):
        itemdef_db = ItemDef.load_items_from_json('data/items/', 'item_list.json', itemdefs=True)
        item_db = ItemDef.load_items_from_json('data/items/', 'item_list.json', itemdefs=False)

        timer.start_timer()
        ItemDef.pack_itemdef_db(itemdef_db, 'data/items', 'item_list.bin')
        times["itemdef"]["pack"].append(float(timer.stop_timer().split(":")[3]))

        timer.start_timer()
        ItemDef.pack_db(item_db, 'data/', 'item_list.bin')
        times["items"]["pack"].append(float(timer.stop_timer().split(":")[3]))

    averages = {
        "itemdef": {
            "pack": sum(times["itemdef"]["pack"])/len(times["itemdef"]["pack"])
        },
        "items": {
            "pack": sum(times["items"]["pack"])/len(times["items"]["pack"])
        }
    }
    return times, averages


if __name__ == '__main__':
    total_passes = 100
    times, averages = pack_unpack_test(total_passes)
    print("Itemdef times over {} passes".format(total_passes))
    print("\tAverage pack times: {} ({}% slower than raw items)".format(averages["itemdef"]["pack"], (averages["itemdef"]["pack"]/averages["items"]["pack"])*100))
    print("Item times over {} passes".format(total_passes))
    print("\tAverage pack times: {}".format(averages["items"]["pack"]))
