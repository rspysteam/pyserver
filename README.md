# PyServer 317 #

### requirement ###
* Python version 3.6.1
* Dill -> link [ https://github.com/uqfoundation/dill/releases/download/0.2.6/dill-0.2.6.zip ]
* 317 RSPS client

### credits ###
Server/game/cache/* credits to [Kyle Fricilone](https://github.com/kfricilone)
